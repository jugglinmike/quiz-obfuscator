# Quiz Obfuscator Contribution Guide

## Set up

1. Install [Node.js](https://nodejs.org) at version 16.17.0 or later
2. Run the command `npm install`

## Automated tests

Run the command `npm test` to execute the project's automated tests.
