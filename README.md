# Quiz Obfuscator

A tool for hiding the answers to quizzes published on the web. Supported sites:

- American Council of Academic Plastic Surgeons

## Installation

<!--
  The contents of this file are visible on both the project's website and from
  the project's source code repository. The instructions on the website should
  include the "built" version of the source code in order to provide viewers
  with a URL to bookmark.

  The following HTML comments allow the build process to insert that link when
  deploying the website. The comments should not be modified without
  corresponding changes to the build process.
-->
<!-- BEGIN INSTALLATION INSTRUCTIONS -->
Visit the project's web page, find the link titled "Obfuscate Quiz", and save
it as a bookmark in your web browser.
<!-- END INSTALLATION INSTRUCTIONS -->

## Usage

Once installed, visit the web page of a quiz where you would like the answers
to be obscured. Use the "Obfuscate Quiz" bookmark to run this application on
the page. This will have the following effects:

- All quiz answers found on the current page will be obscured.
- a button labeled "Reveal answer" will appear next to each quiz answer found
  on the current page. Clicking on this button will reveal that answer.
- A notification will appear describing how many answers have been obscured,
  and it will automatically disappear following a short delay.

## Technical details

This application is designed for use as a "bookmarklet," or a special kind of
web browser bookmark which executes JavaScript on the current page.

This project is free and open source software. You can find its source code and
a contribution guide in [the source code
repository](https://gitlab.com/jugglinmike/quiz-obfuscator).

## License

Copyright 2023 Mike Pennisi under [the GNU General Public License
v3.0](https://www.gnu.org/licenses/gpl-3.0.html)
