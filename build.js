// jshint node: true
'use strict';

const {execSync} = require('child_process');
const path = require('path');
const fs = require('fs/promises');

const commonmark = require('commonmark');
const {minify} = require('uglify-js');

const reader = new commonmark.Parser();
const writer = new commonmark.HtmlRenderer();

const generateHTML = async () => {
	const paths = ['README.md', 'src/index.js']
		.map((name) => path.join(__dirname, name));
	const [markdown, source] = await Promise.all(
		paths.map((path) => fs.readFile(path, 'utf-8'))
	);

	const minifyResult = minify(source);

	if (minifyResult.error) {
		throw new Error(minifyResult.error);
	}

	// Sanitize the JavaScript source code so it is suitable for use as the
	// value of an HTML attribute.
	const sanitized = minifyResult.code.replace(/"/g, '&quot;');

	const installationInstructions = `
  <p>
    To install the Quiz Obfuscator "bookmarklet", save the following link as a bookmark in your web browser:
    <a href="javascript:${sanitized}">Obfuscate Quiz</a>
  </p>
`;
	const body = writer.render(reader.parse(markdown))
		.replace(
			/(<!-- BEGIN INSTALLATION INSTRUCTIONS -->).*(<!-- END INSTALLATION INSTRUCTIONS -->)/s,
			`$1${installationInstructions}$2`
		);

	return `<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Quiz Obfuscator</title>
</head>
<body>${body}</body>
</html>`;
};

(async() => {
	await fs.rm('./public', {recursive: true, force: true});
	await fs.mkdir('public');
	const markup = await generateHTML();
	await fs.writeFile('public/index.html', markup);
})();
