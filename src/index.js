(() => {
	'use strict';
	const obscuredClass = 'quiz-obfuscator-obscured';
	const revealButtonClass = 'quiz-obfuscator-reveal-button';
	const summaryClass = 'quiz-obfuscator-summary';
	// The number of milliseconds for which the summary should be visible
	// before it is automatically removed.
	const summaryLifetime = 5 * 1000;

	// Define the appearance of the text-to-be-obscured and of the alert which
	// explains the result of running this script.
	const style = document.createElement('style');
	style.textContent = `
		.${obscuredClass} {
			background-color: #000 !important;
			color: #000 !important;
		}

		.${summaryClass} {
			position: fixed !important;
			top: 1em !important;
			left: 25% !important;
			z-index: 1000 !important;

			width: 50% !important;
			padding: 1em !important;

			background-color: #fff !important;
			color: #000 !important;
			font-size: 2em !important;
		}
	`;
	document.head.appendChild(style);

	// Find the answers which need to be obscured.
	const found = Array.from(document.querySelectorAll('p'))
		.filter((p) => {
			return /\bthe\s+correct\s+response\s+is\b/i.test(p.textContent);
		});

	// Obscure the answers and any text which follows them. For each set of
	// obscured text, insert a button which will reveal that text when clicked.
	found.forEach((p) => {
			p.classList.add(obscuredClass);

			// Create a button for revealing this answer
			const revealButton = document.createElement('button');
			revealButton.textContent = 'Reveal answer';
			revealButton.classList.add(revealButtonClass);
			revealButton.addEventListener('click', () => {
				revealButton.setAttribute('disabled', true);
				p.classList.remove(obscuredClass);
				let next = p;
				// jshint boss:true
				while (next = next.nextElementSibling) {
					next.classList.remove(obscuredClass);
				}
			});
			p.before(revealButton);

			let next = p;
			// jshint boss:true
			while (next = next.nextElementSibling) {
				next.classList.add(obscuredClass);
			}
		});

	// Explain the result of running this script.
	const summary = document.createElement('div');
	summary.setAttribute('aria-role', 'alert');
	summary.classList.add(summaryClass);
	const answerPlurality = found.length > 1 ? 's' : '';
	summary.textContent = `Obscured ${found.length} answer${answerPlurality}.`;
	document.body.prepend(summary);

	setTimeout(() => summary.remove(), summaryLifetime);
})();
