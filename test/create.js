'use strict';

const fs = require('fs');
const path = require('path');

const {JSDOM} = require('jsdom');

const source = fs.readFileSync(
	path.join(__dirname, '../src/index.js'),
	'utf-8'
);

module.exports = (html) => {
	const dom = new JSDOM(html, {runScripts: 'outside-only'});
	dom.window.eval(source);

	teardown(() => dom.window.close());

	return dom;
};
