'use strict';

const assert = require('assert');
const obscuredClass = 'quiz-obfuscator-obscured';
const summaryClass = 'quiz-obfuscator-summary';

const create = require('../create');

suite('obfuscation', () => {
	suite('single question', () => {
		let dom;

		setup(() => {
			dom = create(`
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head></head>
<body>
<div>
  <p>Question</p>
  <p>
  A) first answer<br>
  B) second answer<br>
  C) third answer<br>
  D) fourth answer<br>
  E) fifth answer
  </p>
  <p>The correct response is Option E.</p>
  <p>Explanation part 1</p>
  <p><b><i>References</i></b></p>
  <ol>
   <li>first reference</li>
   <li>second reference</li>
  </ol>
</div>
</body>
</html>
`);
		});

		test('text', () => {
			const obscuredText = 
				Array.from(dom.window.document.querySelectorAll(`.${obscuredClass}`))
				.map((el) => el.textContent.trim().replace(/\s+/g, ' '));

			assert.deepEqual(
				obscuredText,
				[
					'The correct response is Option E.',
					'Explanation part 1',
					'References',
					'first reference second reference',
				]
			);
		});

		test('summary', () => {
			const summaries = dom.window.document.querySelectorAll(`.${summaryClass}`);

			assert.equal(summaries.length, 1);
			assert.equal(summaries[0].textContent, 'Obscured 1 answer.');
		});
	});


	suite('multiple questions', () => {
		let dom;

		setup(() => {
			dom = create(`
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head></head>
<body>
<div>
  <p>First question</p>
  <p>
  A) first answer<br>
  B) second answer<br>
  C) third answer<br>
  D) fourth answer<br>
  E) fifth answer
  </p>
  <p>The correct response is Option E.</p>
  <p>Explanation 1 part 1</p>
  <p><b><i>References 1</i></b></p>
  <ol>
   <li>first reference 1</li>
   <li>second reference 1</li>
  </ol>
</div>
<div>
  <p>Second question</p>
  <p>
  A) first answer<br>
  B) second answer<br>
  C) third answer<br>
  D) fourth answer<br>
  E) fifth answer
  </p>
  <p>The correct response is Option A.</p>
  <p>Explanation 2 part 1</p>
  <p><b><i>References 2</i></b></p>
  <ol>
   <li>first reference 2</li>
   <li>second reference 2</li>
  </ol>
</div>
</body>
</html>
`);
		});

		test('text', () => {
			const obscuredText = 
				Array.from(dom.window.document.querySelectorAll(`.${obscuredClass}`))
				.map((el) => el.textContent.trim().replace(/\s+/g, ' '));

			assert.deepEqual(
				obscuredText,
				[
					'The correct response is Option E.',
					'Explanation 1 part 1',
					'References 1',
					'first reference 1 second reference 1',
					'The correct response is Option A.',
					'Explanation 2 part 1',
					'References 2',
					'first reference 2 second reference 2',
				]
			);
		});

		test('summary', () => {
			const summaries = dom.window.document.querySelectorAll(`.${summaryClass}`);

			assert.equal(summaries.length, 1);
			assert.equal(summaries[0].textContent, 'Obscured 2 answers.');
		});
	});
});
