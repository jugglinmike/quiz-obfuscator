'use strict';

const assert = require('assert');

const obscuredClass = 'quiz-obfuscator-obscured';
const revealButtonClass = 'quiz-obfuscator-reveal-button';

const create = require('../create');

suite('revelation', () => {
	test('multiple questions', () => {
		const dom = create(`
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head></head>
<body>
<div>
  <p>First question</p>
  <p>
  A) first answer<br>
  B) second answer<br>
  C) third answer<br>
  D) fourth answer<br>
  E) fifth answer
  </p>
  <p>The correct response is Option E.</p>
  <p>Explanation 1 part 1</p>
  <p><b><i>References 1</i></b></p>
  <ol>
   <li>first reference 1</li>
   <li>second reference 1</li>
  </ol>
</div>
<div>
  <p>Second question</p>
  <p>
  A) first answer<br>
  B) second answer<br>
  C) third answer<br>
  D) fourth answer<br>
  E) fifth answer
  </p>
  <p>The correct response is Option A.</p>
  <p>Explanation 2 part 1</p>
  <p><b><i>References 2</i></b></p>
  <ol>
   <li>first reference 2</li>
   <li>second reference 2</li>
  </ol>
</div>
<div>
  <p>Third question</p>
  <p>
  A) first answer<br>
  B) second answer<br>
  C) third answer<br>
  D) fourth answer<br>
  E) fifth answer
  </p>
  <p>The correct response is Option B.</p>
  <p>Explanation 3 part 1</p>
  <p><b><i>References 3</i></b></p>
  <ol>
   <li>first reference 3</li>
   <li>second reference 3</li>
  </ol>
</div>
</body>
</html>
`);
		const revealButtons = Array.from(
			dom.window.document.querySelectorAll(`.${revealButtonClass}`)
		);

		const nextTexts = revealButtons
			.map((button) => button.nextElementSibling.textContent);

		assert.deepEqual(
			nextTexts,
			[
				'The correct response is Option E.',
				'The correct response is Option A.',
				'The correct response is Option B.',
			]
		);

		revealButtons[1].click();

		const disableds = revealButtons
			.map((button) => button.hasAttribute('disabled'));

		assert.deepEqual(disableds, [false, true, false]);

		const obscuredText = 
			Array.from(dom.window.document.querySelectorAll(`.${obscuredClass}`))
			.map((el) => el.textContent.trim().replace(/\s+/g, ' '));

		assert.deepEqual(
			obscuredText,
			[
				'The correct response is Option E.',
				'Explanation 1 part 1',
				'References 1',
				'first reference 1 second reference 1',
				'The correct response is Option B.',
				'Explanation 3 part 1',
				'References 3',
				'first reference 3 second reference 3',
			]
		);
	});
});
